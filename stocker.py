from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score
import yfinance as yf
import matplotlib.pyplot as plt


# Function to fetch historical stock data
def get_stock_data(symbol, start_date, end_date):
    try:
        data = yf.download(symbol, start=start_date, end=end_date)
        return data
    except Exception as e:
        print(f"Error fetching data for {symbol}: {e}")
        return None


# Function to prepare the dataset for machine learning
def prepare_data(data):
    # Assuming 'Close' prices are the target variable, and 'Open', 'High', 'Low', 'Volume' are features
    features = ["Open", "High", "Low", "Volume"]

    # Create target variable: 1 if the next day's closing price is higher, 0 if lower or equal
    data["Target"] = (data["Close"].shift(-1) > data["Close"]).astype(int)

    # Drop rows with NaN values
    data = data.dropna()

    # Features and target variable
    X = data[features]
    y = data["Target"]

    return X, y


# Example usage:
symbol = "AAPL"  # Apple Inc. stock symbol
start_date = "2022-01-01"
end_date = "2023-01-01"

# Get historical stock data
stock_data = get_stock_data(symbol, start_date, end_date)

# Check if data is available
if stock_data is not None:
    # Prepare the dataset for machine learning
    X, y = prepare_data(stock_data)

    # Split the data into training and testing sets
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.2, random_state=42
    )

    # Create a Random Forest classifier
    clf = RandomForestClassifier(n_estimators=100, random_state=42)

    # Train the classifier
    clf.fit(X_train, y_train)

    # Make predictions on the test set
    predictions = clf.predict(X_test)

    # Evaluate the model
    accuracy = accuracy_score(y_test, predictions)
    print(f"Accuracy: {accuracy}")

    # Plot actual vs. predicted values
    plt.figure(figsize=(10, 6))
    plt.plot(y_test.reset_index(drop=True), label="Actual")
    plt.plot(predictions, label="Predicted")
    plt.title("Actual vs. Predicted Values")
    plt.legend()
    plt.show()
else:
    print("No data available.")
